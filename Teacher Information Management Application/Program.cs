﻿class Program
{
    static void Main(string[] args)
    {
        try
        {
            Console.Write("Nhập số lượng giáo viên: ");
            int soLuong = int.Parse(Console.ReadLine());

            if (soLuong <= 0)
            {
                throw new ArgumentException("Số lượng giáo viên phải là một số nguyên dương.");
            }

            List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

            for (int i = 0; i < soLuong; i++)
            {
                Console.WriteLine($"Nhập thông tin cho giáo viên thứ {i + 1}:");
                Console.Write("Họ tên: ");
                string hoTen = Console.ReadLine();
                Console.Write("Năm sinh: ");
                int namSinh = int.Parse(Console.ReadLine());
                Console.Write("Lương cơ bản: ");
                double luongCoBan = double.Parse(Console.ReadLine());
                Console.Write("Hệ số lương: ");
                double heSoLuong = double.Parse(Console.ReadLine());

                GiaoVien gv = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
                danhSachGiaoVien.Add(gv);
            }

            double luongThapNhat = danhSachGiaoVien[0].TinhLuong();
            GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];

            foreach (var gv in danhSachGiaoVien)
            {
                if (gv.TinhLuong() < luongThapNhat)
                {
                    luongThapNhat = gv.TinhLuong();
                    giaoVienLuongThapNhat = gv;
                }
            }

            Console.WriteLine("Thông tin giáo viên có lương thấp nhất:");
            giaoVienLuongThapNhat.XuatThongTin();
        }
        catch (FormatException)
        {
            Console.WriteLine("Lỗi: Định dạng không hợp lệ. Vui lòng nhập đúng định dạng.");
        }
        catch (ArgumentException ex)
        {
            Console.WriteLine($"Lỗi: {ex.Message}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Lỗi không xác định: {ex.Message}");
        }

        Console.WriteLine("Nhấn phím bất kỳ để kết thúc.");
        Console.ReadKey();
    }
}